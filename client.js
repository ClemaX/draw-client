const io = require('socket.io-client');
const request = require('request');

request.post('http://localhost:8080/users/authenticate', {
    form: {
        username: process.argv[2], 
        password: process.argv[3]
    }
}, (error, response, body) => {
    if (error) {
        console.error(error);
    } else if ((response && response.statusCode != 200) || !body) {
        console.error('Unknown error: ' + response.statusCode);
    } else {
        console.log('Logged in!');
        const user = JSON.parse(body);
        connect(user.token);
    }
});

function connect(token) {
    const socket = io.connect('http://localhost:8080', {
        query: { token: token }
    });

    socket.on('authenticated', () => {
        console.log('Authenticated');

        if(process.argv[4]) {
            console.log('Joining room ' + process.argv[4]);
            socket.emit('joinRoom', { id: process.argv[4] });
        } else {
            console.log('Searching room...');
            socket.emit('searchRoom');
        }

        socket.on('drawer', (data) => {
            console.log('Drawer: ' + data.id);
        });
    });
    
    socket.on('room', (data) => {
        console.log('Joined room: ' + data.id);
    });
    
    socket.on('error', (data) => {
        console.error(data);
    });
}




